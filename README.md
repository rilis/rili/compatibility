# Rili Compatibility

This is module of [rili](https://gitlab.com/rilis/rili) which provide stuff  which allow rili to be cross compiler and cross platfrom

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/compatibility)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/compatibility/badges/master/build.svg)](https://gitlab.com/rilis/rili/compatibility/commits/master)